import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>Exercices</h1>
    <mat-tab-group>
      <mat-tab label="Obs1">
        <app-timer></app-timer>
      </mat-tab>
      <mat-tab label="Obs2"><app-obs2></app-obs2></mat-tab>
      <mat-tab label="Obs3"><app-obs3></app-obs3></mat-tab>
      <mat-tab label="Ex1">
        <app-exercice1></app-exercice1>
      </mat-tab>
      <mat-tab label="Ex2"><app-ex2></app-ex2></mat-tab>
      <mat-tab label="Ex3"><app-ex3></app-ex3></mat-tab>
      <mat-tab label="Ex4"><app-ex4></app-ex4></mat-tab>
      <mat-tab label="Ex5"><app-ex5></app-ex5></mat-tab>
    </mat-tab-group>
  `,
  styles: [`
    h1 {
      color: pink;
    }
  `]
})
export class AppComponent {
  title = 'demo';
}
