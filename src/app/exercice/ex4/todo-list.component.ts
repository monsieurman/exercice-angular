import { Component, Input } from '@angular/core';
import { Todo } from '../model';

@Component({
  selector: 'app-todo-list',
  template: `
    <ul>
      <li *ngFor="let todo of todos">{{todo.content}}</li>
    </ul>
  `
})
export class TodoListComponent {
  @Input()
  todos: Todo[] = []
}
