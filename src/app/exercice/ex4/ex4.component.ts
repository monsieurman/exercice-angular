import { Component } from '@angular/core';
import { Todo } from '../model';

@Component({
  selector: 'app-ex4',
  template: `
    <app-create-todo (createTodo)="createTodo($event)"></app-create-todo>
    <app-todo-list [todos]="todos"></app-todo-list>
    <button (click)="clearTodos()">CLEAR</button>
  `
})
export class Ex4Component {
  todos: Todo[] = [
    {
      id: 0,
      content: 'Faire la vaisselle'
    }
  ];

  createTodo(content: string) {
    this.todos = [
      ...this.todos,
      {
        id: Math.random(),
        content: content
      }
    ];
  }

  clearTodos() {
    this.todos = [];
  }
}
