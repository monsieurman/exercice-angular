import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-create-todo',
  template: `
    <input type="text" [(ngModel)]="todo">
    <button (click)="onClick()">CREATE</button>
  `
})
export class CreateTodoComponent {
  todo = '';

  @Output()
  createTodo = new EventEmitter<string>();

  onClick() {
    if (this.todo !== '') {
      this.createTodo.emit(this.todo);
      this.todo = '';
    }
  }
}
