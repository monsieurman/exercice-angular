import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../model';

@Component({
  selector: 'app-todo-list2',
  template: `
    <ul>
      <li *ngFor="let todo of todos">
        <app-todo-list-item 
          [todo]="todo" 
          (deleteTodo)="deleteTodo.emit($event)"
        >
        </app-todo-list-item>
      </li>
    </ul>
  `
})
export class TodoList2Component {
  @Input()
  todos: Todo[] = [];

  @Output()
  deleteTodo = new EventEmitter<number>();
}
