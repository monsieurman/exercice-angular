import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Todo } from '../model';

@Component({
  selector: 'app-todo-list-item',
  template: `
    <ng-template [ngIf]="todo" [ngIfElse]="elseBlock">
      <span>{{todo.content}}</span>
      <button (click)="deleteTodo.emit(todo.id)">
        DELETE
      </button>
    </ng-template>
    <ng-template #elseBlock>
      Pas de todo
    </ng-template>
  `
})
export class TodoListItemComponent {
  @Input()
  todo?: Todo;

  @Output()
  deleteTodo = new EventEmitter<number>();
}
