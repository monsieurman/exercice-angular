import { Component, OnInit } from '@angular/core';
import { Todo } from '../model';

@Component({
  selector: 'app-ex5',
  template: `
    <app-create-todo 
      (createTodo)="createTodo($event)"
    >
    </app-create-todo>
    <app-todo-list2 
      [todos]="todos" 
      (deleteTodo)="deleteTodo($event)"
    >
    </app-todo-list2>
    <button (click)="clearTodos()">CLEAR</button>
  `
})
export class Ex5Component {
  todos: Todo[] = [
    {
      id: 0,
      content: 'Faire la vaisselle'
    }
  ];

  createTodo(content: string) {
    this.todos = [
      ...this.todos,
      {
        id: Math.random(),
        content: content
      }
    ];
  }

  deleteTodo(id: number) {
    this.todos = this.todos
      .filter(todo => todo.id !== id);
  }

  clearTodos() {
    this.todos = [];
  }
}
