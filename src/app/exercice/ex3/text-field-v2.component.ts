import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-text-field-v2',
  template: `
    <input 
      #inputRef
      type="text" 
      [value]="message"
      (input)="messageChange.emit(inputRef.value)"
    >
  `
})
export class TextFieldV2Component  {
  @Input()
  message = '';

  @Output()
  messageChange = new EventEmitter<string>();
}
