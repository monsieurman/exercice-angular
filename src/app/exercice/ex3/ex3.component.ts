import { Component } from '@angular/core';

@Component({
  selector: 'app-ex3',
  template: `
    <app-text-field-v2 
      [message]="message" 
      (messageChange)="message = $event"
      >
    </app-text-field-v2>
    <app-text-field-v2 [message]="message" (messageChange)="message = $event"></app-text-field-v2>
  `
})
export class Ex3Component {
  message = 'Hello la mundo';
}
