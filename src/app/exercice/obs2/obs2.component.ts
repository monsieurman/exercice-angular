import { Component } from '@angular/core';

@Component({
  selector: 'app-obs2',
  template: `
    <app-sync-input></app-sync-input>
    <app-sync-input></app-sync-input>
    <app-sync-input></app-sync-input>
    <app-sync-input></app-sync-input>
    <app-sync-input></app-sync-input>
    <app-sync-input></app-sync-input>
  `
})
export class Obs2Component {

}
