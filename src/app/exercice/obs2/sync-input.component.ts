import { Component } from '@angular/core';
import { SyncService } from './sync.service';

@Component({
  selector: 'app-sync-input',
  template: `
    <input 
      #inputRef
      type="text" 
      (input)="onInput(inputRef.value)" 
      [value]="value"
    >
  `,
})
export class SyncInputComponent {
  value = '';

  constructor(
    public syncService: SyncService
  ) { 
    this.syncService.value.subscribe(v => this.value = v);
  }

  onInput(input: string) {
    this.syncService.update(input);
  }
}
