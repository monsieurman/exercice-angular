import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SyncService {
  private _value = new BehaviorSubject('');
  public value = this._value.asObservable();

  public update(value: string) {
    this._value.next(value)
  }
}
