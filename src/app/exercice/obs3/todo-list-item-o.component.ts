import { Component, Input } from '@angular/core';
import { Todo } from '../model';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-todo-list-item-o',
  template: `
    <!-- Affichage conditionelle du template avec *ngIf -->
    <!-- Ici pour vérifier qu'on a bien une valeur dans la variable todo -->
    <ng-template [ngIf]="todo" [ngIfElse]="elseBlock">
      <span>{{todo.content}}</span>

      <!-- On appelle la méthode deleteTodo du service au click du bouton -->
      <button 
        (click)="todoService.deleteTodo(todo.id)"
      >
        DELETE
      </button>
    </ng-template>
    <ng-template #elseBlock>
      Pas de todo
      <!-- En théorie, cette partie de template n'est jamais afficher si le todo est bien passé en paramètre -->
    </ng-template>
  `
})
export class TodoListItemOComponent {
  // On prend une variable todo en paramètre, voir utilisation dans todo-list
  @Input()
  todo?: Todo;

  constructor(
    // Injection de dépendance, angular créer une instance globale du service et s'occupe de nous la donner.
    public todoService: TodoService
  ) { }
}
