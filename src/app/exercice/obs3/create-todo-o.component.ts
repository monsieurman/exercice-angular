import { Component } from '@angular/core';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-create-todo-o',
  template: `
    <!-- ngModel nous permet de binder la variable todo à la valeur de l'input -->
    <input 
      type="text" 
      (keyup.enter)="createTodo()"
      [(ngModel)]="todo"
    >
    <!-- On appelle la méthode createTodo à l'appuie de la touche entré, ou au click du bouton -->
    <button (click)="createTodo()">CREATE</button>
  `
})
export class CreateTodoOComponent {
  todo = '';
  
  constructor(
    // Injection de dépendance, angular créer une instance globale du service et s'occupe de nous la donner.
    private todoService: TodoService
  ) { }

  createTodo() {
    // Si la variable n'est pas vide
    if (this.todo !== '') {
      // On appelle le service pour créer un todo
      this.todoService.createTodo(this.todo);
      this.todo = '';
    }
  }
}
