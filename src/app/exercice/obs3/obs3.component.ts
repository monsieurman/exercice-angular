import { Component } from '@angular/core';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-obs3',
  template: `
    <!-- Les composants étant indépendant, on a qu'à les ajouter dans le template -->
    <app-create-todo-o></app-create-todo-o>
    <app-todo-list-o></app-todo-list-o>
    <button (click)="todoService.clearTodos()">CLEAR</button>
  `
})
export class Obs3Component {
  constructor(
    // Injection de dépendance, angular créer une instance globale du service et s'occupe de nous la donner.
    public todoService: TodoService
  ) { }
}
