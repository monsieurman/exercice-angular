import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Todo } from '../model';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  // On créé un BehaviorSubject avec un tableau vide en valeur par défaut
  private _todos = new BehaviorSubject<Todo[]>([]);
  // Qu'on expose au reste de l'application au travers de cette variable public
  // La méthode asObservable permet de "cacher" les méthodes .next() .value etc... au reste du programme
  public todos = this._todos.asObservable();

  clearTodos() {
    // On doit pousser un nouveau tableau à chaque fois
    // Ici on pousse un tableau vide avec .next()
    this._todos.next([]);
  }

  createTodo(content: string) {
    // On créé un nouveau tableau
    const newValue: Todo[] = [
      // En copiant l'ancien tableau contenu dans .value
      ...this._todos.value,
      // Et en ajoutant un nouveau todo ensuite
      {
        content,
        id: Math.random()
      }
    ];

    // Puis on pousse le nouveau tableau dans l'observable.
    this._todos.next(newValue);
  }

  deleteTodo(id: number) {
    const newTodos = this._todos.value
      // On utilise filter pour créer un nouveau tableau en ne gardant que les id différentes de celle passer en paramètre
      // C'est à dire supprimer le todo ayant cette id.
      .filter(t => t.id !== id);

    // Puis on pousse le nouveau tableau dans l'observable.
    this._todos.next(newTodos);
  }
}
