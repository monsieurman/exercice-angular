import { Component } from '@angular/core';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-todo-list-o',
  template: `
    <ul>
      <!-- On s'abonne à l'observable grace au | async -->
      <!-- Pour itérer dessus avec la directive *ngFor -->
      <li *ngFor="let todo of (todoService.todos | async)">
        <!-- On passe ensuite le todo en paramètre au composant todo-list-item -->
        <app-todo-list-item-o 
          [todo]="todo" 
        >
        </app-todo-list-item-o>
      </li>
    </ul>
  `
})
export class TodoListOComponent {
  constructor(
    // Injection de dépendance, angular créer une instance globale du service et s'occupe de nous la donner.
    public todoService: TodoService
  ) { }
}
