import { Component } from '@angular/core';

@Component({
  selector: 'app-text-field',
  template: `
    <input type="text" [(ngModel)]="message" />
    <p>{{ message }}</p>
  `
})
export class TextFieldComponent  {
  message = '';
}
