import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { Exercice1Component } from './exercice/exercice1.component';
import { Ex2Component } from './exercice/ex2/ex2.component';
import { TextFieldComponent } from './exercice/ex2/text-field.component';
import { Ex3Component } from './exercice/ex3/ex3.component';
import { TextFieldV2Component } from './exercice/ex3/text-field-v2.component';
import { Ex4Component } from './exercice/ex4/ex4.component';
import { CreateTodoComponent } from './exercice/ex4/create-todo.component';
import { TodoListComponent } from './exercice/ex4/todo-list.component';
import { Ex5Component } from './exercice/ex5/ex5.component';
import { TodoList2Component } from './exercice/ex5/todo-list2.component';
import { TodoListItemComponent } from './exercice/ex5/todo-list-item.component';
import { TimerComponent } from './observable/timer.component';
import { SyncInputComponent } from './exercice/obs2/sync-input.component';
import { Obs2Component } from './exercice/obs2/obs2.component';
import { Obs3Component } from './exercice/obs3/obs3.component';
import { CreateTodoOComponent } from './exercice/obs3/create-todo-o.component';
import { TodoListOComponent } from './exercice/obs3/todo-list-o.component';
import { TodoListItemOComponent } from './exercice/obs3/todo-list-item-o.component';

@NgModule({
  declarations: [
    AppComponent,
    Exercice1Component,
    Ex2Component,
    TextFieldComponent,
    Ex3Component,
    TextFieldV2Component,
    Ex4Component,
    CreateTodoComponent,
    TodoListComponent,
    Ex5Component,
    TodoList2Component,
    TodoListItemComponent,
    TimerComponent,
    SyncInputComponent,
    Obs2Component,
    Obs3Component,
    CreateTodoOComponent,
    TodoListOComponent,
    TodoListItemOComponent
  ],
  imports: [
    BrowserModule,
    MatTabsModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
