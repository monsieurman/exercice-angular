import { Component } from '@angular/core';
import { TimerService } from './timer.service';

@Component({
  selector: 'app-timer',
  template: `
    <h1>Temps écoulé</h1>
    <p>{{ elapsedTime }}</p>
    <p>{{ toto.elapsedTime | async }}</p>
    <!-- <p>{{ date | date }}</p> -->
  `
})
export class TimerComponent {
  elapsedTime = 0;
  date = new Date();

  constructor(
    public toto: TimerService
  ) { 
    this.toto.elapsedTime.subscribe(
      v => this.elapsedTime = v
    )
  }
}
