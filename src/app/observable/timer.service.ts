import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  private _elapsedTime = new BehaviorSubject(0);

  public elapsedTime = this._elapsedTime.asObservable();

  constructor() {
    setInterval(() => {
      this._elapsedTime.next(
        this._elapsedTime.value + 1
      );
    }, 1000);
  }
}
